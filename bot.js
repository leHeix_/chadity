const { Client } = require('klasa');
const config = require('./global/config.json');
require('dotenv').config()

new Client({
    clientOptions: {
        fetchAllMembers: false
    },
    prefix: config.defaultPrefix,
    ignoreBots: true,
    cmdLogging: false,
    typing: true,
    ownerID: '468232873959358474',
    readyMessage: (client) => '\r\n'.repeat(9999) + `---> ${config.botname} <--- \n└ Guilds: ${client.guilds.size}\n└ Users: ${client.users.size - 1}` + '\r\n'.repeat(14) + `[-- LOGGING --]`
}).login(process.env.TOKEN);