const { Command } = require('klasa');
const { MessageEmbed } = require('discord.js');
const config = require('../global/config.json');

module.exports = class extends Command {

    constructor(...args) {
        super(...args, {
            name: 'restart',
            enabled: true,
            runIn: ['text', 'dm', 'group'],
            cooldown: 0,
            permLevel: 9,
            botPerms: [],
            requiredSettings: [],
            description: 'Restart the bot.',
            quotedStringSupport: false, 
            usage: '',
            usageDelim: undefined,
            extendedHelp: `Restarts the bot.`
        });
    }

    async run(message) {
        const display = new MessageEmbed()
        .setColor('RANDOM')
        .setAuthor(this.client.user.username, this.client.user.avatarURL())
        .setTitle(message.language.get('RESTART_TITLE'))
        .setDescription(message.language.get('RESTART_DESCRIPTION'))
        .setTimestamp()
        .setFooter(message.language.get('REQUESTED', message.author.username), message.author.avatarURL());

        await message.send(display);
        this.client.destroy();
        process.exit(1);
    }
}
