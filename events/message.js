const { Event, KlasaConsole } = require('klasa');

module.exports = class extends Event {

	run(message) {
        var console = new KlasaConsole();
		if (this.client.ready) {
            this.client.monitors.run(message);
            if(message.author.id == this.client.user.id) return;
            console.log(`[MESSAGE ${message.id} - #${message.channel.name} in ${message.channel.guild.name}] ${message.author.username}#${message.author.discriminator}: ${message.content}`);
        }
	}

};